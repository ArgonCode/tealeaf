def grandma
=begin
Write a Deaf Grandma program. 
Whatever you say to grandma (whatever you type in), 
she should respond with  HUH?!  SPEAK UP, SONNY!, 
unless you shout it (type in all capitals). 
If you shout, she can hear you (or at least she thinks so) 
and yells back, NO, NOT SINCE 1938! To make your program 
really believable, have grandma shout a different year each time;
maybe any year at random between 1930 and 1950. 
(This part is optional, and would be much easier 
if you read the section on Ruby's random number 
generator at the end of the methods chapter.) 
You can't stop talking to grandma until you shout BYE.
=end
  your_text = ""

  while your_text != "BYE"
    puts "Say sth to grandma! Do not be a weirdo!"
    your_text = gets.chomp
    if your_text.upcase == your_text
      random_year = rand(1900..2015)
      puts "NO, NOT SINCE #{random_year}!"
    else
      puts "HUH?! SPEAK UP, SONNY!"
    end
  end
end

def grandma_extended
=begin
What if grandma doesn't want you to leave? When you shout BYE, she could pretend not to hear you. Change your previous program so that you have to shout BYE three times in a row. Make sure to test your program: if you shout BYE three times, but not in a row, you should still be talking to grandma.
=end
  counter = 0
  your_text = ""

  while counter != 3
    puts "Say sth to grandma! Do not be a weirdo!"
    your_text = gets.chomp

    if your_text == "BYE"
      counter += 1
    elsif your_text.upcase == your_text
      random_year = rand(1900..2015)
      counter = 0
      puts "NO, NOT SINCE #{random_year}!"
    else
      counter = 0
      puts "HUH?! SPEAK UP, SONNY!"
    end

  end
end

# run all:
grandma
grandma_extended