=begin
Leap Years. Write a program which will ask for a starting year and an ending year, and then puts all of the leap years between them (and including them, if they are also leap years). Leap years are years divisible by four (like 1984 and 2004). However, years divisible by 100 are not leap years (such as 1800 and 1900) unless they are divisible by 400 (like 1600 and 2000, which were in fact leap years). (Yes, it's all pretty confusing, but not as confusing has having July in the middle of the winter, which is what would eventually happen.)
=end  

def leap_year
  puts "Please give me a start year"
  start_year = gets.chomp.to_i

  while start_year < 1582
    puts "Please give me at least 1582"
    start_year = gets.chomp.to_i
  end

  puts "Please give me an end year"
  end_year = gets.chomp.to_i

  while end_year < start_year
    puts "End year has to be no smaller than than the start year. I did not make the rules! Sorry!"
    end_year = gets.chomp.to_i
  end

  current_year = start_year

  puts "All leap years between #{start_year} and #{end_year} are:"

  while current_year <= end_year
    div_4 = current_year % 4 == 0
    div_100 = current_year % 100 == 0
    div_400 = current_year % 400 == 0

    if div_400
      print "#{current_year}, "
    elsif div_100
      # skip
    elsif div_4
      print "#{current_year}, "
    end  

    current_year += 1

  end
  puts ""
end
    
leap_year